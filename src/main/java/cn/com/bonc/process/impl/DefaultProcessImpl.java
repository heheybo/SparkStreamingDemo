/**
 * FileName: DefaultProcessImpl
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/2/15 11:18
 * Description:其他默认过滤实现类，这里可以添加你自己的过滤代码实现；
 * 同样你也可以创建一个新的类并实现Filter接口，实现过滤
 */
package cn.com.bonc.process.impl;

import cn.com.bonc.process.Process;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class DefaultProcessImpl implements Process {

    private Dataset<Row> outDataset;

    public DefaultProcessImpl(Dataset<Row> outDataset) {
        this.outDataset = outDataset;
    }

    @Override
    public Dataset<Row> processing(Dataset<Row> rowDataset) {

        return rowDataset;
    }

}
