/**
 * FileName: DefaultfilterImpl
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/2/14 12:40
 * Description: 不对数据内容处理，只是单纯的实现将单列数据转换为多列
 */
package cn.com.bonc.process.impl;

import cn.com.bonc.process.Process;
import cn.com.bonc.util.ColumnsUtil;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

public class SingleColum2MutiProcessImpl implements Process {
    @Override
    public Dataset<Row> processing(Dataset<Row> rowDataset) {
        return ColumnsUtil.getInstance().getMultiColumnDataset(rowDataset,"value");
    }
}
