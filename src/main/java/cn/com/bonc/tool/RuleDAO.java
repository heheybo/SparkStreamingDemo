package cn.com.bonc.tool;


import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 * Created by ${RQL} on 2019/1/9
 */
public interface RuleDAO<T> {
    //public  boolean  formulate(T t) throws Exception;
    public Dataset<Row> formulate(T t) throws Exception;
}
