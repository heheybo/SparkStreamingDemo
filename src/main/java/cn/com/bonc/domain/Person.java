package cn.com.bonc.domain;

/***
 * 用于封装被关联的数据的关键字段
 */
public class Person {

    /** 名字 */
    private String name;

    /** 身份证号码 */
    private String certId;

    public Person(String name, String certId) {
        this.name = name;
        this.certId = certId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCertId() {
        return certId;
    }

    public void setCertId(String certId) {
        this.certId = certId;
    }

}
