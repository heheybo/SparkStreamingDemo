/**
 * FileName: Source
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/3/20 17:41
 * Description:
 */
package cn.com.bonc.source;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;

public interface Source<T> {
    Dataset<T> getDataset(SparkSession sparkSession);
}