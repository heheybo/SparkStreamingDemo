/**
 * FileName: AssociatedHDFSSourceImpl
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/3/20 19:27
 * Description:
 */
package cn.com.bonc.source.impl;

import cn.com.bonc.conf.ConfigurationManager;
import cn.com.bonc.constant.Constants;
import cn.com.bonc.source.Source;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SparkSession;

public class AssociatedHDFSSourceImpl implements Source<String>{
    @Override
    public Dataset<String> getDataset(SparkSession sparkSession) {
        return sparkSession.readStream()
                .textFile(ConfigurationManager.getProperty(Constants.HDFS_STATIC_PATH));
    }
}
