/**
 * FileName: AssociatedRedisSourceImpl
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/3/20 17:44
 * Description:
 */
package cn.com.bonc.source.impl;

import cn.com.bonc.source.Source;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

public class AssociatedRedisSourceImpl implements Source<Row>{
    @Override
    public Dataset<Row> getDataset(SparkSession sparkSession) {
        return sparkSession.read()
                .format("org.apache.spark.sql.redis")
                .option("table", "Xdata")
                .load();
    }
}
