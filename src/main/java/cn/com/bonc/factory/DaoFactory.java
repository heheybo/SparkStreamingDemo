package cn.com.bonc.factory;

import cn.com.bonc.rules.*;
import cn.com.bonc.tool.RuleDAO;

/**
 * Created by ${RQL} on 2019/1/9
 */
public class DaoFactory {



        public static final int rule1 = 1;
        public static final int rule2 = 2;
        public static final int rule3 = 3;
        public static final int rule4 = 4;
        public static final int rule5 = 5;
        //public static final int rule6 = 6;

        public static   RuleDAO getUpdateDAO(int type) {
            switch (type){
                case rule1:
                    return new Rule1();
                case rule2:
                    return  new Rule2();
                case rule3:
                    return  new Rule3();
                case rule4:
                    return  new Rule4();
                case rule5:
                    return  new Rule5();
                default:
                    return new Rule6();
            }

        }



}
