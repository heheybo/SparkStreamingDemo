/**
 * FileName: OperatorEnum
 * Author:   SAMSUNG-PC 孙中军
 * Date:     2019/2/14 10:10
 * Description:
 */
package cn.com.bonc.constant;

public enum OperatorEnum {

    //rule.json中支持的操作
    LIKE("LK"),//正则匹配
    EQUAL("EQ"),//等于
    NOT_EQUAL("NE"),//不等于
    GREATER_THAN("GT"),//大于
    LESS_THAN("LT"),//小于
    GREATER_THAN_OR_EQUAL("GE"),//大于等于
    LESS_THAN_OR_EQUAL("LE"),//小于等于
    DEFAULT(""),//其他情况

    //action.json中支持的操作
    REPLACE_STRING("REP"), //替换字符串
    ADD_STRING("ADD"), //在原有字符串上添加
    DELETE_COLUMN("DLC"),//将字符串内容清空
    DROP_ROW("DPR");//将该行记录彻底清除



    private String operatorName;

    OperatorEnum(String operatorName) {
        this.operatorName = operatorName;
    }


    /**
     * 根据类型的名称，返回类型的枚举实例。
     * @param operatorName 类型名称
     */
    public static OperatorEnum fromOperatorName(String operatorName) {
        for (OperatorEnum Operator : OperatorEnum.values()) {
            if (Operator.getOperatorName().equals(operatorName)) {
                return Operator;
            }
        }
        return null;
    }

    public String getOperatorName() {
        return operatorName;
    }
}
