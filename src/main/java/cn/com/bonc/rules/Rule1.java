package cn.com.bonc.rules;

import cn.com.bonc.tool.RuleDAO;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 * Created by ${RQL} on 2019/1/9
 */
public class Rule1 implements RuleDAO<Dataset<Row>> {


    @Override
    public Dataset<Row> formulate(Dataset<Row> o) throws Exception {
        o.filter((FilterFunction<Row>) x -> {
            String phone = x.getAs("phone");
            //phone.startsWith("+86") ?  phone.replace("+86","") : phone
            if (phone.startsWith("+86")){
                return true;
            }else
                phone =  phone.replace("+86","");

            return true;


        });
        return o;
    }
}

