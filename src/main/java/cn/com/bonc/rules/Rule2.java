package cn.com.bonc.rules;

import cn.com.bonc.tool.RuleDAO;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;

/**
 * Created by ${RQL} on 2019/1/9
 */
public class Rule2 implements RuleDAO<Dataset<Row>> {
    @Override
    public Dataset<Row> formulate(Dataset<Row> t) throws Exception {
        t.filter((FilterFunction<Row>) x -> {
            String Phone = x.getAs("phone");
            if (Phone.length() == 11)
            {
                return true;
            }else
                //TODO
            return  false;

        });
        return t;
    }
}
/*Dataset<Row> res = rowDataset.filter((FilterFunction<Row>) x -> {
			String userAgent = x.getAs("userAgent");
			if (userAgent!=""){
				if (userAgent.indexOf("MicroMessenger") > -1)
					return true;
			}
			return false;
		});
*/
